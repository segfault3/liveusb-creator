% tails-installer(1) tails-installer user manual
% This manual page was written by anonym <anonym@riseup.net>
% September 25, 2017

NAME
====

`tails-installer` - installs and upgrades Tails on USB drives

SYNOPSIS
========

	tails-installer

DESCRIPTION
===========

`tails-installer` is a graphical interface that allows the user to
pick a Tails ISO image and install it to a USB drive, or upgrade an
existing Tails installation (without destroying any persistent
data). If `tails-installer` is run inside Tails, it can use the
running Tails instance as the source data for the installation,
removing the need for a separate Tails ISO image.
