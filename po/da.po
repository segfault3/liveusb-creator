# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Adam Jerjir Ancker <nrbbiyoutube@gmail.com>, 2013
# Anders Damsgaard <andersd@riseup.net>, 2013
# bna1605 <bna1605@gmail.com>, 2014
# David Nielsen <gnomeuser@gmail.com>, 2014
# Jesper Heinemeier <jesper@jx2.dk>, 2015
# Joe Hansen <joedalton2@yahoo.dk>, 2013
# Kris Thomsen <lakristho@gmail.com>, 2009
# mort3n <nalholm@yahoo.dk>, 2013
# Niels Nielsen Horn <Niels@codingpirates.dk>, 2015
# Per Hansen <per@kofod-hansen.com>, 2014
# scootergrisen, 2017-2019
# 7110bbed2db4fadc642a4cd0e91bb1b9, 2015
msgid ""
msgstr ""
"Project-Id-Version: Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-13 08:42+0200\n"
"PO-Revision-Date: 2019-02-24 19:20+0000\n"
"Last-Translator: scootergrisen\n"
"Language-Team: Danish (http://www.transifex.com/otf/torproject/language/"
"da/)\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../tails_installer/creator.py:100
msgid "You must run this application as root"
msgstr "Du skal køre dette program som root"

#: ../tails_installer/creator.py:146
msgid "Extracting live image to the target device..."
msgstr "Udpakker det direkte foto til den valgte enhed..."

#: ../tails_installer/creator.py:153
#, python-format
msgid "Wrote to device at %(speed)d MB/sec"
msgstr "Skrev til enhed med %(speed)d MB/s"

#: ../tails_installer/creator.py:296
#, python-format
msgid ""
"There was a problem executing the following command: `%(command)s`.\n"
"A more detailed error log has been written to '%(filename)s'."
msgstr ""
"Der opstod et problem under kørsel af følgende kommando: \"%(command)s\".\n"
"En mere detaljeret fejllog er blevet skrevet til \"%(filename)s\"."

#: ../tails_installer/creator.py:315
msgid "Verifying SHA1 checksum of LiveCD image..."
msgstr "Verificerer SHA1-kontrolsum af LiveCD-aftrykket..."

#: ../tails_installer/creator.py:319
msgid "Verifying SHA256 checksum of LiveCD image..."
msgstr "Verificerer SHA256-kontrolsum af LiveCD-aftrykket..."

#: ../tails_installer/creator.py:335
msgid ""
"Error: The SHA1 of your Live CD is invalid.  You can run this program with "
"the --noverify argument to bypass this verification check."
msgstr ""
"Fejl: SHA1'et på din live-CD er ugyldig. Du kan køre dette program med \"--"
"noverify\"-argumentet for at springe over denne verificeringskontrol."

#: ../tails_installer/creator.py:341
msgid "Unknown ISO, skipping checksum verification"
msgstr "Ukendt ISO, undlader verifikation af kontrolsum"

#: ../tails_installer/creator.py:353
#, python-format
msgid ""
"Not enough free space on device.\n"
"%dMB ISO + %dMB overlay > %dMB free space"
msgstr ""
"Der er ikke tilstrækkelig ledig plads på enheden.\n"
"%dMB ISO + %dMB ekstra > %dMB fri plads"

#: ../tails_installer/creator.py:360
#, python-format
msgid "Creating %sMB persistent overlay"
msgstr "Opretter %sMB vedvarende overlæg"

#: ../tails_installer/creator.py:421
#, python-format
msgid "Unable to copy %(infile)s to %(outfile)s: %(message)s"
msgstr "Kan ikke kopiere %(infile)s til %(outfile)s: %(message)s"

#: ../tails_installer/creator.py:435
msgid "Removing existing Live OS"
msgstr "Fjerner eksisterende Live OS"

#: ../tails_installer/creator.py:444 ../tails_installer/creator.py:457
#, python-format
msgid "Unable to chmod %(file)s: %(message)s"
msgstr "Kan ikke chmod %(file)s: %(message)s"

#: ../tails_installer/creator.py:450
#, python-format
msgid "Unable to remove file from previous LiveOS: %(message)s"
msgstr "Kan ikke fjerne fil fra tidligere LiveOS: %(message)s"

#: ../tails_installer/creator.py:464
#, python-format
msgid "Unable to remove directory from previous LiveOS: %(message)s"
msgstr "Kan ikke fjerne mappe fra tidligere LiveOS: %(message)s"

#: ../tails_installer/creator.py:512
#, python-format
msgid "Cannot find device %s"
msgstr "Kan ikke finde enhed %s"

#: ../tails_installer/creator.py:713
#, python-format
msgid "Unable to write on %(device)s, skipping."
msgstr "Kan ikke skrive til %(device)s, springer over."

#: ../tails_installer/creator.py:743
#, python-format
msgid ""
"Some partitions of the target device %(device)s are mounted. They will be "
"unmounted before starting the installation process."
msgstr ""
"Nogle partitioner på den valgte enhed %(device)s er i brug. De vil alle "
"blive skubbet ud før installationen starter."

#: ../tails_installer/creator.py:786 ../tails_installer/creator.py:1010
msgid "Unknown filesystem.  Your device may need to be reformatted."
msgstr "Ukendt filsystem. Din enhed skal måske formateres igen."

#: ../tails_installer/creator.py:789 ../tails_installer/creator.py:1013
#, python-format
msgid "Unsupported filesystem: %s"
msgstr "Filsystem understøttes ikke: %s"

#: ../tails_installer/creator.py:807
#, python-format
msgid "Unknown GLib exception while trying to mount device: %(message)s"
msgstr "Ukendt GLib-undtagelse ved forsøg på at montere enhed: %(message)s"

#: ../tails_installer/creator.py:812
#, python-format
msgid "Unable to mount device: %(message)s"
msgstr "Kan ikke tilslutte enhed: %(message)s"

#: ../tails_installer/creator.py:817
msgid "No mount points found"
msgstr "Fandt ingen tilslutningspunkter"

#: ../tails_installer/creator.py:828
#, python-format
msgid "Entering unmount_device for '%(device)s'"
msgstr "Starter afmontering for \"%(device)s\""

#: ../tails_installer/creator.py:838
#, python-format
msgid "Unmounting mounted filesystems on '%(device)s'"
msgstr "Afmonterer monterede filsystemer på \"%(device)s\""

#: ../tails_installer/creator.py:842
#, python-format
msgid "Unmounting '%(udi)s' on '%(device)s'"
msgstr "Afmonterer \"%(udi)s\" på \"%(device)s\""

#: ../tails_installer/creator.py:853
#, python-format
msgid "Mount %s exists after unmounting"
msgstr "Tilslutning %s eksisterer efter frakobling"

#: ../tails_installer/creator.py:866
#, python-format
msgid "Partitioning device %(device)s"
msgstr "Partitionerer enheden %(device)s"

#: ../tails_installer/creator.py:995
#, python-format
msgid "Unsupported device '%(device)s', please report a bug."
msgstr "Enheden \"%(device)s\" understøttes ikke. Rapportér venligst en fejl."

#: ../tails_installer/creator.py:998
msgid "Trying to continue anyway."
msgstr "Prøver at forsætte alligevel."

#: ../tails_installer/creator.py:1007 ../tails_installer/creator.py:1405
msgid "Verifying filesystem..."
msgstr "Verificerer filsystem..."

#: ../tails_installer/creator.py:1031
#, python-format
msgid "Unable to change volume label: %(message)s"
msgstr "Kan ikke ændre drev-navn: %(message)s"

#: ../tails_installer/creator.py:1037 ../tails_installer/creator.py:1440
msgid "Installing bootloader..."
msgstr "Installerer opstartsindlæser..."

#: ../tails_installer/creator.py:1064
#, python-format
msgid "Could not find the '%s' COM32 module"
msgstr "Kunne ikke finde '%s' COM32-modulet"

#: ../tails_installer/creator.py:1072 ../tails_installer/creator.py:1458
#, python-format
msgid "Removing %(file)s"
msgstr "Fjerner %(file)s"

#: ../tails_installer/creator.py:1186
#, python-format
msgid "%s already bootable"
msgstr "%s er allerede i stand til at starte"

#: ../tails_installer/creator.py:1206
msgid "Unable to find partition"
msgstr "Kan ikke finde partition"

#: ../tails_installer/creator.py:1229
#, python-format
msgid "Formatting %(device)s as FAT32"
msgstr "Formatterer %(device)s som FAT32"

#: ../tails_installer/creator.py:1289
msgid "Could not find syslinux' gptmbr.bin"
msgstr "Kunne ikke finde syslinux' gptmbr.bin"

#: ../tails_installer/creator.py:1302
#, python-format
msgid "Reading extracted MBR from %s"
msgstr "Læser udtrukket MBR fra %s"

#: ../tails_installer/creator.py:1306
#, python-format
msgid "Could not read the extracted MBR from %(path)s"
msgstr "Kunne ikke læse den udtrukne MBR fra %(path)s"

#: ../tails_installer/creator.py:1319 ../tails_installer/creator.py:1320
#, python-format
msgid "Resetting Master Boot Record of %s"
msgstr "Nulstiller Master Boot Record for %s"

#: ../tails_installer/creator.py:1325
msgid "Drive is a loopback, skipping MBR reset"
msgstr "Drevet er et loopback, springer nulstilling af MBR over"

#: ../tails_installer/creator.py:1329 ../tails_installer/creator.py:1589
#, python-format
msgid "Calculating the SHA1 of %s"
msgstr "Beregner SHA1 for %s"

#: ../tails_installer/creator.py:1354
msgid "Synchronizing data on disk..."
msgstr "Synkroniserer data på disk..."

#: ../tails_installer/creator.py:1397
msgid "Error probing device"
msgstr "Fejl ved undersøgelse af enhed"

#: ../tails_installer/creator.py:1399
msgid "Unable to find any supported device"
msgstr "Kan ikke finde nogen understøttede enheder"

#: ../tails_installer/creator.py:1409
msgid ""
"Make sure your USB key is plugged in and formatted with the FAT filesystem"
msgstr "Sørg for at din USB-nøgle er sæt i og formateret med FAT-filsystemet"

#: ../tails_installer/creator.py:1412
#, python-format
msgid ""
"Unsupported filesystem: %s\n"
"Please backup and format your USB key with the FAT filesystem."
msgstr ""
"Filsystem understøttes ikke: %s\n"
"Foretag venligst en sikkerhedskopi og formatér din USB-nøgle med FAT-"
"filsystemet."

#: ../tails_installer/creator.py:1481
msgid ""
"Unable to get Win32_LogicalDisk; win32com query did not return any results"
msgstr ""
"Kan ikke få Win32_LogicalDisk; win32com forespørgslen returnerede ingen "
"resultater"

#: ../tails_installer/creator.py:1536
msgid "Cannot find"
msgstr "Kan ikke finde"

#: ../tails_installer/creator.py:1537
msgid ""
"Make sure to extract the entire tails-installer zip file before running this "
"program."
msgstr ""
"Sørg for at udtrække hele tails-installationsprogram zip-filen inden dette "
"program køres."

#: ../tails_installer/gui.py:69
#, python-format
msgid "Unknown release: %s"
msgstr "Ukendt udgivelse: %s"

#: ../tails_installer/gui.py:73
#, python-format
msgid "Downloading %s..."
msgstr "Downloader %s..."

#: ../tails_installer/gui.py:213
msgid ""
"Error: Cannot set the label or obtain the UUID of your device.  Unable to "
"continue."
msgstr ""
"Fejl: Kan ikke sætte etiket eller finde enhedens UUID. Kan ikke fortsætte."

#: ../tails_installer/gui.py:260
#, python-format
msgid "Installation complete! (%s)"
msgstr "Installation færdig! (%s)"

#: ../tails_installer/gui.py:265
msgid "Tails installation failed!"
msgstr "Tails installation mislykkedes!"

#: ../tails_installer/gui.py:369
msgid ""
"Warning: This tool needs to be run as an Administrator. To do this, right "
"click on the icon and open the Properties. Under the Compatibility tab, "
"check the \"Run this program as an administrator\" box."
msgstr ""
"Advarsel: Dette værktøj skal køres som en administrator. For dette, "
"højreklik på ikonet og åbn egenskaberne. Under kompatibilitetsfanebladet "
"afkrydses boksen »Kør dette program som en administrator«."

#: ../tails_installer/gui.py:381
msgid "Tails Installer"
msgstr "Tails-installationsprogram"

#: ../tails_installer/gui.py:441
msgid "Tails Installer is deprecated in Debian"
msgstr "Tails-installationsprogram er udgået i Debian"

#: ../tails_installer/gui.py:443
msgid ""
"To install Tails from scratch, use GNOME Disks instead.\n"
"<a href='https://tails.boum.org/install/linux/usb-overview'>See the "
"installation instructions</a>\n"
"\n"
"To upgrade Tails, do an automatic upgrade from Tails or a manual upgrade "
"from Debian using a second USB stick.\n"
"<a href='https://tails.boum.org/upgrade/tails-overview'>See the manual "
"upgrade instructions</a>"
msgstr ""
"Brug i stedet GNOME Diske for at installere Tails fra bunden.\n"
"<a href='https://tails.boum.org/install/linux/usb-overview'>Se "
"instruktionerne til installation</a>\n"
"\n"
"Opgrader Tails ved at foretage en automatisk opgradering fra Tails eller en "
"manual opgradering fra Debian med en anden USB-pen.\n"
"<a href='https://tails.boum.org/upgrade/tails-overview'>Se instruktionerne "
"til manuel opgradering</a>"

#: ../tails_installer/gui.py:450 ../data/tails-installer.ui.h:2
msgid "Clone the current Tails"
msgstr "Klon den nuværende Tails"

#: ../tails_installer/gui.py:457 ../data/tails-installer.ui.h:3
msgid "Use a downloaded Tails ISO image"
msgstr "Brug et downloadet Tails ISO-aftryk"

#: ../tails_installer/gui.py:494 ../tails_installer/gui.py:815
msgid "Upgrade"
msgstr "Opgrader"

#: ../tails_installer/gui.py:496
msgid "Manual Upgrade Instructions"
msgstr "Instruktioner til manuel opgradering"

#: ../tails_installer/gui.py:498
msgid "https://tails.boum.org/upgrade/"
msgstr "https://tails.boum.org/upgrade/"

#: ../tails_installer/gui.py:506 ../tails_installer/gui.py:727
#: ../tails_installer/gui.py:792 ../data/tails-installer.ui.h:7
msgid "Install"
msgstr "Installer"

#: ../tails_installer/gui.py:509 ../data/tails-installer.ui.h:1
msgid "Installation Instructions"
msgstr "Installationsinstruktioner"

#: ../tails_installer/gui.py:511
msgid "https://tails.boum.org/install/"
msgstr "https://tails.boum.org/install/"

#: ../tails_installer/gui.py:517
#, python-format
msgid "%(size)s %(vendor)s %(model)s device (%(device)s)"
msgstr "%(size)s %(vendor)s %(model)s enhed (%(device)s)"

#: ../tails_installer/gui.py:529
msgid "No ISO image selected"
msgstr "Ingen ISO-aftryk valgt"

#: ../tails_installer/gui.py:530
msgid "Please select a Tails ISO image."
msgstr "Vælg venligst et Tails ISO-aftryk."

#: ../tails_installer/gui.py:572
msgid "No device suitable to install Tails could be found"
msgstr "Der kunne ikke findes nogen egnet enhed til installation af Tails"

#: ../tails_installer/gui.py:574
#, python-format
msgid "Please plug a USB flash drive or SD card of at least %0.1f GB."
msgstr "Sæt venligst et USB-flash-drev eller SD-kort i på mindst %0.1f GB."

#: ../tails_installer/gui.py:608
#, fuzzy, python-format
msgid ""
"The USB stick \"%(pretty_name)s\" is configured as non-removable by its "
"manufacturer and Tails will fail to start from it. Please try installing on "
"a different model."
msgstr ""
"USB-pennen \"%(pretty_name)s\" er konfigureret som ikke-flytbar af dens "
"producent og Tails vil ikke kunne start fra den. Prøv venligst at installere "
"en anden model."

#: ../tails_installer/gui.py:618
#, python-format
msgid ""
"The device \"%(pretty_name)s\" is too small to install Tails (at least "
"%(size)s GB is required)."
msgstr ""
"Enheden \"%(pretty_name)s\" har ikke plads nok til at installere Tails "
"(kræver mindst %(size)s GB)."

#: ../tails_installer/gui.py:631
#, python-format
msgid ""
"To upgrade device \"%(pretty_name)s\" from this Tails, you need to use a "
"downloaded Tails ISO image:\n"
"https://tails.boum.org/install/download"
msgstr ""
"For at opgradere enheden \"%(pretty_name)s\" fra denne Tails, skal du bruge "
"et downloadet Tails ISO-aftryk:\n"
"https://tails.boum.org/install/download"

#: ../tails_installer/gui.py:652
msgid "An error happened while installing Tails"
msgstr "Der opstod fejl ved installation af Tails"

#: ../tails_installer/gui.py:664
msgid "Refreshing releases..."
msgstr "Genopfrisker udgivelser..."

#: ../tails_installer/gui.py:669
msgid "Releases updated!"
msgstr "Udgivelser opdateret!"

#: ../tails_installer/gui.py:722
msgid "Installation complete!"
msgstr "Installation færdig!"

#: ../tails_installer/gui.py:738
msgid "Cancel"
msgstr "Annullér"

#: ../tails_installer/gui.py:774
msgid "Unable to mount device"
msgstr "Kan ikke tilslutte enhed"

#: ../tails_installer/gui.py:781 ../tails_installer/gui.py:814
msgid "Confirm the target USB stick"
msgstr "Bekræft mål USB-pennen"

#: ../tails_installer/gui.py:782
#, python-format
msgid ""
"%(size)s %(vendor)s %(model)s device (%(device)s)\n"
"\n"
"All data on this USB stick will be lost."
msgstr ""
"%(size)s %(vendor)s %(model)s enhed (%(device)s)\n"
"\n"
"Al data på denne USB-pen vil gå tabt."

#: ../tails_installer/gui.py:801
#, python-format
msgid "%(parent_size)s %(vendor)s %(model)s device (%(device)s)"
msgstr "%(parent_size)s %(vendor)s %(model)s enhed (%(device)s)"

#: ../tails_installer/gui.py:809
msgid ""
"\n"
"\n"
"The persistent storage on this USB stick will be preserved."
msgstr ""
"\n"
"\n"
"Det vedvarende lager på denne USB-pen vil blive bevaret."

#: ../tails_installer/gui.py:810
#, python-format
msgid "%(description)s%(persistence_message)s"
msgstr "%(description)s%(persistence_message)s"

#: ../tails_installer/gui.py:853
msgid "Download complete!"
msgstr "Download udført!"

#: ../tails_installer/gui.py:857
msgid "Download failed: "
msgstr "Download fejlede: "

#: ../tails_installer/gui.py:858
msgid "You can try again to resume your download"
msgstr "Du kan prøve at genoptage din download"

#: ../tails_installer/gui.py:866
msgid ""
"The selected file is unreadable. Please fix its permissions or select "
"another file."
msgstr ""
"Den valgte fil er ikke læsbar. Du kan enten ændre filtilladelserne eller "
"vælge en anden fil."

#: ../tails_installer/gui.py:872
msgid ""
"Unable to use the selected file.  You may have better luck if you move your "
"ISO to the root of your drive (ie: C:\\)"
msgstr ""
"Kan ikke bruge den valgte fil. Prøv i stedet at flytte din ISO til roden af "
"dit drev (dvs. C:\\)"

#: ../tails_installer/gui.py:878
#, python-format
msgid "%(filename)s selected"
msgstr "%(filename)s valgt"

#: ../tails_installer/source.py:29
msgid "Unable to find LiveOS on ISO"
msgstr "Kan ikke finde LiveOS på ISO"

#: ../tails_installer/source.py:35
#, python-format
msgid "Could not guess underlying block device: %s"
msgstr "Kunne ikke gætte underliggende blokenhed: %s"

#: ../tails_installer/source.py:50
#, python-format
msgid ""
"There was a problem executing `%s`.\n"
"%s\n"
"%s"
msgstr ""
"Der opstod et problem ved eksekvering af `%s`.\n"
"%s\n"
"%s"

#: ../tails_installer/source.py:64
#, python-format
msgid "'%s' does not exist"
msgstr "'%s' findes ikke"

#: ../tails_installer/source.py:66
#, python-format
msgid "'%s' is not a directory"
msgstr "'%s' er ikke en mappe"

#: ../tails_installer/source.py:76
#, python-format
msgid "Skipping '%(filename)s'"
msgstr "Springer '%(filename)s' over"

#: ../tails_installer/utils.py:55
#, python-format
msgid ""
"There was a problem executing `%s`.%s\n"
"%s"
msgstr ""
"Der opstod et problem ved eksekvering af `%s`.%s\n"
"%s"

#: ../tails_installer/utils.py:135
msgid "Could not open device for writing."
msgstr "Kunne ikke åbne enhed til skrivning."

#: ../data/tails-installer.ui.h:4
msgid "Select a distribution to download:"
msgstr "Vælg en distribution som skal downloades:"

#: ../data/tails-installer.ui.h:5
msgid "Target USB stick:"
msgstr "Mål USB-pen:"

#: ../data/tails-installer.ui.h:6
msgid "Reinstall (delete all data)"
msgstr "Geninstaller (slet al data)"
